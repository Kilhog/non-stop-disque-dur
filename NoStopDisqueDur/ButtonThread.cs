﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace NoStopDisqueDur
{
    class ButtonThread
    {
        public bool Stop = true;
        public int TempSleep = 5000;
        public string LettreDisque = "d";

        public static readonly int m = 100000000;
        public static readonly int ml = 10000;
        public static readonly int b = 31415821;
        static int a = 1;

        public void Beta()
        {
            while (true)
            {
                if (Stop == false)
                {
                    int test = Randome();
                    //string text = System.IO.File.ReadAllText(@"D:\NoStopDisqueDur.txt");
                    System.IO.StreamWriter file = new System.IO.StreamWriter(LettreDisque+":\\NoStopDisqueDur.txt");
                    file.WriteLine(test.ToString());

                    file.Close();
                }
            }
        }
        public int Mult(int p, int q)
        {
            int p1, p0, ql, q0;
            p1 = p / ml;
            p0 = p % ml;
            ql = q / ml;
            q0 = q % ml;
            return (((p0 * ql + p1 * q0) % ml) * ml + p0 * q0) % m;
        }

        public int Randome()
        {
            Thread.Sleep(TempSleep);
            a = (Mult(a, b) + 1) % m;
            return a;
        }
    }
}
