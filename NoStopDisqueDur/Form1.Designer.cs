﻿namespace NoStopDisqueDur
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Titre = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.TextLecteur = new System.Windows.Forms.TextBox();
            this.TextSleep = new System.Windows.Forms.TextBox();
            this.BoutonLecteur = new System.Windows.Forms.Button();
            this.BoutonSleep = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Titre
            // 
            this.Titre.AutoSize = true;
            this.Titre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Titre.Location = new System.Drawing.Point(10, 9);
            this.Titre.Name = "Titre";
            this.Titre.Size = new System.Drawing.Size(393, 20);
            this.Titre.TabIndex = 0;
            this.Titre.Text = "Bienvenue sur l\'application NoStopDisqueDur";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(14, 42);
            this.button1.Name = "button1";
            this.button1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button1.Size = new System.Drawing.Size(195, 58);
            this.button1.TabIndex = 1;
            this.button1.Text = "Executer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "V1.1 By El\'Pimousso";
            // 
            // TextLecteur
            // 
            this.TextLecteur.Location = new System.Drawing.Point(215, 42);
            this.TextLecteur.Name = "TextLecteur";
            this.TextLecteur.Size = new System.Drawing.Size(14, 20);
            this.TextLecteur.TabIndex = 3;
            // 
            // TextSleep
            // 
            this.TextSleep.Location = new System.Drawing.Point(215, 80);
            this.TextSleep.Name = "TextSleep";
            this.TextSleep.Size = new System.Drawing.Size(41, 20);
            this.TextSleep.TabIndex = 4;
            // 
            // BoutonLecteur
            // 
            this.BoutonLecteur.Location = new System.Drawing.Point(328, 42);
            this.BoutonLecteur.Name = "BoutonLecteur";
            this.BoutonLecteur.Size = new System.Drawing.Size(75, 23);
            this.BoutonLecteur.TabIndex = 5;
            this.BoutonLecteur.Text = "OK";
            this.BoutonLecteur.UseVisualStyleBackColor = true;
            // 
            // BoutonSleep
            // 
            this.BoutonSleep.Location = new System.Drawing.Point(327, 76);
            this.BoutonSleep.Name = "BoutonSleep";
            this.BoutonSleep.Size = new System.Drawing.Size(75, 23);
            this.BoutonSleep.TabIndex = 6;
            this.BoutonSleep.Text = "OK";
            this.BoutonSleep.UseVisualStyleBackColor = true;
            this.BoutonSleep.Click += new System.EventHandler(this.BoutonSleep_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(230, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = ":/";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(257, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "ms";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 135);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BoutonSleep);
            this.Controls.Add(this.BoutonLecteur);
            this.Controls.Add(this.TextSleep);
            this.Controls.Add(this.TextLecteur);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Titre);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "NonStopDisqueDur - v1.1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Titre;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextLecteur;
        private System.Windows.Forms.TextBox TextSleep;
        private System.Windows.Forms.Button BoutonLecteur;
        private System.Windows.Forms.Button BoutonSleep;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

