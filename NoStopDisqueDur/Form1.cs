﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace NoStopDisqueDur
{

    public partial class Form1 : Form
    {
        Thread oThread;
        ButtonThread oAlpha;
        public Form1()
        {
            InitializeComponent();

            oAlpha = new ButtonThread();
            oThread = new Thread(new ThreadStart(oAlpha.Beta));

            TextLecteur.Text = oAlpha.LettreDisque;
            TextSleep.Text = oAlpha.TempSleep.ToString();

            oThread.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if((sender as Control).Text == "Stopper")
            {
                (sender as Control).Text = "Reprendre";

                oAlpha.Stop = true;
            }
            else
            {
                (sender as Control).Text = "Stopper";

                oAlpha.Stop = false;
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            oThread.Abort();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void BoutonSleep_Click(object sender, EventArgs e)
        {
            oAlpha.TempSleep = int.Parse(TextSleep.Text);
        }
    }
}
